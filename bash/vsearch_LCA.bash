#!/bin/bash

source $1
INPUT=$2
TMP=$(mktemp --tmpdir="tmp/")
OUTPUT=$(echo $INPUT | \
	awk -v outputdir=$OUTPUTDIR -F"/" '{sub(/.fas(ta)*(\.gz)*/,".vsearch.out.lca.gz",$NF)
                        print outputdir"/"$NF}')

# look for hits
vsearch --usearch_global ${INPUT} \
            -db ${REFDB} \
            --id 0.80 \
            --maxaccepts 0 \
	        --userout $TMP \
	        --threads $THREADS \
            --iddef 1 \
            --userfields "query+id1+target"
	    
awk -v threshold=$THRESHOLD -f awk/LCA.awk $TAXO $TMP | \
	gzip > $OUTPUT

OUTPUT=$(echo $INPUT | \
	awk -v outputdir=$OUTPUTDIR -F"/" '{sub(/.fas(ta)*(\.gz)*/,".vsearch.out.besthit.gz",$NF)
                        print outputdir"/"$NF}')

awk -v threshold=100 -f awk/LCA.awk $TAXO $TMP | \
	gzip > $OUTPUT

rm -f $TMP
