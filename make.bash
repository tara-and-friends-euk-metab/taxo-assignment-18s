#!/bin/bash

###################################################################
# script name   : make.bash
# description   : This script performs a taxonomic assignment
#                 of 18S ASVs/OTUs 
# usage         : sbatch make.sh but better to run the lines one
#                 by one and check manualy the outputs
# author	: Nicolas Henry
# contact	: nicolas.henry@cnrs.fr
###################################################################

############################################
# Prepare directory
############################################

sbatch --wait analyses/00_dir_prep.bash

sleep 2

############################################
# Reference database preparation
############################################

sbatch --wait analyses/01_prepare_refdb.bash

sleep 2

############################################
# Taxonomic assignment with vsearch
############################################

sbatch --wait \
    --export=CONFIG="config/taxo_assign_vsearch_pr2.cfg" \
	analyses/02_taxo_assign_vsearch_a.bash