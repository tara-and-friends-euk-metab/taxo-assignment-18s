BEGIN{FS=sep;OFS="\t"}

!/>/{
    gsub("U","T")
    print > outseq
}

/>/{
    id=$1
    sub(/>/,"",id)
    sub("^[^"sep"]+"sep,"",$0)
    gsub(taxosep,";",$0)
    print id,$0 > outtaxo
    print ">"id > outseq
}