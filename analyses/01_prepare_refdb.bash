#!/bin/bash




########################################################
# Download the references db
########################################################

# PR2
wget -N https://github.com/pr2database/pr2database/releases/download/v4.14.0/pr2_version_4.14.0_SSU_UTAX.fasta.gz -P data/refdb/
wget -N https://github.com/pr2database/pr2database/releases/download/v4.14.0/pr2_version_4.14.0_SSU.decipher.trained.rds -P data/refdb/

# EukRibo

# https://doi.org/10.5281/zenodo.6327891 (downloaded manually)

########################################################
# Clean output/refdb
########################################################

rm -f outputs/refdb/*

########################################################
# Reference database formating
########################################################

# vsearch

bash/format_for_vsearch.bash data/refdb/46345_EukRibo_V4_2020-10-27.fas.gz ' ' '\\|'
bash/format_for_vsearch.bash data/refdb/34438_EukRibo_V9_2020-10-27.fas.gz ' ' '\\|'
bash/format_for_vsearch.bash data/refdb/pr2_version_4.14.0_SSU_UTAX.fasta.gz ';tax=' ','

# idtaxa

ln data/refdb/pr2_version_4.14.0_SSU.decipher.trained.rds outputs/refdb/pr2_version_4.14.0_SSU.decipher.trained.rds

########################################################
# Reference database formating
########################################################

bash/ref_db_trimm.bash config/ref_db_trimm_pr2_18SV4.cfg
bash/ref_db_trimm.bash config/ref_db_trimm_pr2_18SV9.cfg