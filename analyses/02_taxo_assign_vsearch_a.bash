#!/bin/bash
#
#SBATCH --partition long
#SBATCH --cpus-per-task 1
#SBATCH --mem 30GB

source $CONFIG

# tmp dir for chunks
mkdir -p tmp/chunks
rm -f tmp/chunks/*

# split the big fasta file into several chunks
# for parallel jobs
zcat -f $FASTA | split -l 10 --filter='gzip > $FILE.fas.gz' - tmp/chunks/file

# list the chunk files
find ./tmp/chunks -name "*.fas.gz" -type f ! -empty > tmp/chunk_files.txt

# VSEARCH LCA

# One job per chunk is launched
sbatch --wait \
	--array 1-$(cat tmp/chunk_files.txt | wc -l)%50 \
	--export=CONFIG=${CONFIG} \
	--cpus-per-task $THREADS \
	analyses/02_taxo_assign_vsearch_b.bash

sleep 10

# Put te files together
sbatch --export=CONFIG=${CONFIG} \
	analyses/02_taxo_assign_vsearch_c.bash